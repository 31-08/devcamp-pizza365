const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8006;

app.use(express.static(__dirname + '/view'))

app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/view/pizza365index.html"))
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})